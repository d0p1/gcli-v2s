# Examples of gcli commands for copy-paste

## Configuration

It can be handful to use Ǧcli with a configuration file to avoid passing arguments on every command.

```sh
# show config commands
gcli config
# show where config file is stored
gcli config where
# save config to use gdev network for next commands
gcli --network gdev config save
# save config to use Alice predefined account
gcli -S predefined -s Alice config save
# the arguments above can be combined
# command below sets local network and predefined secret
gcli --network local -S predefined -s Alice config save
```

In the following, we assume this last command was run. More about the config in [config.md](./config.md).

## Commands

Here is a list of useful commands

```sh
# get duniter current block
gcli blockchain current-block
# get balance of configured account
gcli account balance
# get identity information without indexer
gcli --no-indexer identity get -a 5Hn2LeMZXPFitMwrmrGucwtAPSLEiP4o5zTF7kHzMBtEkJUr 
# get information about Alice identity (needs indexer)
gcli identity get --username Alice
# claim universal dividends
gcli ud claim
# transfer 5000 units
gcli account transfer 5000 5E4i8vcNjnrDp21Sbnp32WHm2gz8YP3GGFwmdpfg5bHd8Whb
```

For testing purpose it can be useful to submit extrinsic without waiting for result and create block manually. 

```sh
# only submit extrinsic to network and do not listen to result
gcli --no-wait account transfer 1234 5FeggKqw2AbnGZF9Y9WPM2QTgzENS3Hit94Ewgmzdg5a3LNa
# create block manually (with manual sealing)
gcli blockchain create-block
```

## Indexer commands

You can check first that indexer is on the same network as Duniter node:

```sh
# check if indexer is on the same chain as duniter
gcli indexer check
```

The following commands uniquely relate with indexer.

```sh
# show latest indexer indexed block
gcli indexer latest-block
```