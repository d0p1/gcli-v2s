# Ğcli config 

Some Ğcli commands require to have an address configured (for example to get account balance), some require to have a secret configured (to sign extrinsics).
Ğcli allows to save the address you want in a config file and to overwrite parts in command line arguments. Example:

```sh
# save Alice address to config file
gcli -S predefined -s Alice config save

# show config 
gcli config show
# [stdout]
# Ğcli config
# duniter endpoint ws://localhost:9944
# indexer endpoint http://localhost:4350/graphql
# address 5GrwvaEF5zXb26Fz9rcQpDWS57CtERHpNehXCPcNoHGKutQY

# use different address in command line
gcli --address 5Fxune7f71ZbpP2FoY3mhYcmM596Erhv1gRue4nsPwkxMR4n config show
# [stdout]
# Ğcli config
# duniter endpoint ws://localhost:9944
# indexer endpoint http://localhost:4350/graphql
# address 5Fxune7f71ZbpP2FoY3mhYcmM596Erhv1gRue4nsPwkxMR4n
```

This also applies to rpc endpoint config (`--url`) or indexer config (`--indexer`).

## Using password encrypted vault

For convenience, default accounts are hardcoded in Ğcli without needing a password:

```sh
# when Alice address is stored in config file
gcli account transfer 1 5Fxune7f71ZbpP2FoY3mhYcmM596Erhv1gRue4nsPwkxMR4n
# no need for password to sign transaction
```

but in general usage, you want to store your secret in the local vault. This goes like this:

```sh
# list available keys in the vault
gcli vault list
# [stdout]
# available keys:
# 5DfhGyQdFobKM8NsWvEeAKk5EQQgYe9AydgJ7rMB6E1EqRzV

# add a new secret to the vault
gcli vault import
# [stdout]
# Mnemonic: <enter mnemonic> 
# Password: <enter password>
```

After saving your secret to the vault, you will be able to unlock it with the password:

```sh
gcli account transfer 123 5Fxune7f71ZbpP2FoY3mhYcmM596Erhv1gRue4nsPwkxMR4n
# [stdout]
# Enter password to unlock account 5DfhGyQdFobKM8NsWvEeAKk5EQQgYe9AydgJ7rMB6E1EqRzV
# Password: <enter password>
# transaction submitted to the network, waiting 6 seconds...
# transfered 1.23 ĞD (5DfhGyQdFobKM8NsWvEeAKk5EQQgYe9AydgJ7rMB6E1EqRzV → 5Fxune7f71ZbpP2FoY3mhYcmM596Erhv1gRue4nsPwkxMR4n)
```

You can display the secret files location:

```sh
gcli vault where
# [stdout]
# /home/hugo/.local/share/gcli
```