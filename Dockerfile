# FROM rustlang/rust:nightly-slim as build
# nightly needed for -Zgit=shallow-deps build but ahash problem with stdsimd
FROM rust:latest as build
# # Use musl Rust image
# FROM clux/muslrust:1.75.0-stable as build

WORKDIR /app

# Copy the Cargo.toml and Cargo.lock files to leverage Docker's caching mechanism
COPY Cargo.toml Cargo.lock ./

# Build the dependencies of the application separately
RUN --mount=type=cache,target=/app/target \
    --mount=type=cache,target=/root/.cargo/registry \
    mkdir src \
    && echo "fn main() {}" > src/main.rs \
    && cargo build --release \
    && rm -r src

# Copy the rest of the source code
COPY ./res ./res
COPY ./src ./src

# Build the application
RUN cargo build --release

# Start release stage
FROM debian as release

# Set the working directory to the location of the built application
WORKDIR /app

# Copy the built application from the previous stage
COPY --from=build /app/target/release/gcli .

# Specify the entrypoint for the container
ENTRYPOINT ["/app/gcli"]