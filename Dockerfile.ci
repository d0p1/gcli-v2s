# Stage 1: Build environment with all dependencies
FROM rust:slim-bookworm as builder

# Install necessary dependencies
RUN apt-get update && apt-get install -y \
    clang \
    llvm \
    make \
    file \
    git \
    curl \
    cmake \
    python3 \
    python3-dev \
    python3-pip

# Clone osxcross repository and set up
RUN git clone https://github.com/tpoechtrager/osxcross.git /opt/osxcross && \
    cd /opt/osxcross && \
    ./tools/get_dependencies.sh && \
    cd /opt/osxcross/tarballs && \
    curl -LO https://github.com/joseluisq/macosx-sdks/releases/download/10.12/MacOSX10.12.sdk.tar.xz && \
    cd /opt/osxcross && \
    UNATTENDED=yes ./build.sh

# Stage 2: Create the final image with only the necessary files
FROM rust:slim-bookworm

# Install cargo-bundle
RUN cargo install cargo-bundle

RUN apt-get update && apt-get install -y --no-install-recommends clang zip; \
    apt-get clean; \
    rm -rf /var/lib/apt/lists/*;

# Copy cargo plugins from the builder stage
COPY --from=builder /opt/osxcross/target /opt/osxcross/target

# Set environment variables for osxcross and Rust
ENV PATH="/opt/osxcross/target/bin:/usr/local/cargo/bin:$PATH"
ENV CC="o64-clang"
ENV CXX="o64-clang++"

# Add rust target for macOS
RUN rustup target add x86_64-apple-darwin

# Copy scripts
COPY scripts/install_gcli.scpt /opt/install_gcli.scpt
COPY scripts/Info.plist /opt/Info.plist

# Define the entrypoint
CMD ["bash"]
