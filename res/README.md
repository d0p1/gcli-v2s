# Resources

## Graphql schema and queries for Duniter indexer

Update the schema with:

```sh
# install graphql client cli
cargo install graphql_client_cli
# download schema from node
graphql-client introspect-schema https://subsquid.gdev.coinduf.eu/graphql --output ./res/indexer-schema.json
```

...

## Metadata

To update the scale-encoded Duniter metadata, spawn a node and run the subxt command.

```sh
# install subxt
cargo install subxt
# spawn a node listening on localhost:9944
duniter --dev
# fetch the metadata with subxt
subxt metadata -f bytes > res/metadata.scale
```
