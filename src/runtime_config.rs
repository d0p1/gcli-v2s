// #[allow(clippy::enum_variant_names)]
#[cfg(feature = "gdev")]
#[subxt::subxt(
	runtime_metadata_path = "res/metadata.scale",
	derive_for_all_types = "Debug"
)]
pub mod runtime {}

// declare custom types
pub type Client = subxt::OnlineClient<Runtime>;
pub type AccountId = subxt::utils::AccountId32;
pub type IdtyId = u32;
pub type BlockNumber = u32;
pub type TxProgress = subxt::tx::TxProgress<Runtime, Client>;
pub type Balance = u64;
pub type AccountData =
	runtime::runtime_types::pallet_duniter_account::types::AccountData<Balance, IdtyId>;
pub type AccountInfo = runtime::runtime_types::frame_system::AccountInfo<u32, AccountData>;
pub type Hash = sp_core::H256;

// declare runtime types
pub enum Runtime {}
impl subxt::config::Config for Runtime {
	type AssetId = ();
	type Hash = Hash;
	type AccountId = AccountId;
	type Address = sp_runtime::MultiAddress<Self::AccountId, u32>;
	type Signature = sp_runtime::MultiSignature;
	type Hasher = subxt::config::substrate::BlakeTwo256;
	type Header = subxt::config::substrate::SubstrateHeader<BlockNumber, Self::Hasher>;
	type ExtrinsicParams = subxt::config::DefaultExtrinsicParams<Self>;
}

// Tip for transaction fee
#[derive(Copy, Clone, Debug, Default, codec::Encode)]
pub struct Tip {
	#[codec(compact)]
	tip: u64,
}
impl Tip {
	pub fn new(amount: u64) -> Self {
		Tip { tip: amount }
	}
}
impl From<u64> for Tip {
	fn from(n: u64) -> Self {
		Self::new(n)
	}
}
