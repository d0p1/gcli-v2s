use crate::*;
use std::str;

// display events in a friendly manner

pub trait DisplayEvent {
	fn display(&self, data: &Data) -> String;
}
impl DisplayEvent for runtime::universal_dividend::events::UdsClaimed {
	fn display(&self, data: &Data) -> String {
		format!(
			"claimed {} UD, for a total of {}",
			self.count,
			data.format_balance(self.total)
		)
	}
}
impl DisplayEvent for runtime::certification::events::CertAdded {
	fn display(&self, _data: &Data) -> String {
		format!("new certification {} → {}", self.issuer, self.receiver)
	}
}
impl DisplayEvent for runtime::certification::events::CertRenewed {
	fn display(&self, _data: &Data) -> String {
		format!("renewed cert {:?}", self)
	}
}
impl DisplayEvent for runtime::account::events::AccountUnlinked {
	fn display(&self, _data: &Data) -> String {
		format!("account unlinked: {}", self.0)
	}
}
impl DisplayEvent for runtime::technical_committee::events::Voted {
	fn display(&self, _data: &Data) -> String {
		format!("voted {:?}", self)
	}
}
impl DisplayEvent for runtime::identity::events::IdtyCreated {
	fn display(&self, _data: &Data) -> String {
		format!(
			"identity created for {} with index {}",
			self.owner_key, self.idty_index
		)
	}
}
impl DisplayEvent for runtime::identity::events::IdtyConfirmed {
	fn display(&self, _data: &Data) -> String {
		format!(
			"identity confirmed with name \"{}\" (index {}, owner key {})",
			str::from_utf8(&self.name.0).unwrap(),
			self.idty_index,
			self.owner_key
		)
	}
}
impl DisplayEvent for runtime::identity::events::IdtyChangedOwnerKey {
	fn display(&self, _data: &Data) -> String {
		format!("identity changed owner key {:?}", self)
	}
}
impl DisplayEvent for runtime::distance::events::EvaluationRequested {
	fn display(&self, _data: &Data) -> String {
		format!("evaluation requested {:?}", self)
	}
}
impl DisplayEvent for runtime::smith_members::events::InvitationSent {
	fn display(&self, _data: &Data) -> String {
		format!("sent smith invitation {:?}", self)
	}
}
impl DisplayEvent for runtime::smith_members::events::InvitationAccepted {
	fn display(&self, _data: &Data) -> String {
		format!("accepted smith invitation {:?}", self)
	}
}
impl DisplayEvent for runtime::smith_members::events::SmithCertAdded {
	fn display(&self, _data: &Data) -> String {
		format!("new smith certification {:?}", self)
	}
}
impl DisplayEvent for runtime::smith_members::events::SmithMembershipAdded {
	fn display(&self, _data: &Data) -> String {
		format!("new smith promoted {:?}", self)
	}
}
impl DisplayEvent for runtime::identity::events::IdtyRemoved {
	fn display(&self, _data: &Data) -> String {
		format!("identity removed {:?}", self)
	}
}
impl DisplayEvent for runtime::account::events::AccountLinked {
	fn display(&self, _data: &Data) -> String {
		format!("account {} linked to identity {}", self.who, self.identity)
	}
}
impl DisplayEvent for runtime::oneshot_account::events::OneshotAccountCreated {
	fn display(&self, _data: &Data) -> String {
		format!("oneshot {:?}", self)
	}
}
impl DisplayEvent for runtime::oneshot_account::events::OneshotAccountConsumed {
	fn display(&self, _data: &Data) -> String {
		format!("oneshot {:?}", self)
	}
}
impl DisplayEvent for runtime::authority_members::events::MemberGoOnline {
	fn display(&self, _data: &Data) -> String {
		format!("smith went online {:?}", self)
	}
}
impl DisplayEvent for runtime::authority_members::events::MemberGoOffline {
	fn display(&self, _data: &Data) -> String {
		format!("smith went offline {:?}", self)
	}
}
impl DisplayEvent for runtime::sudo::events::KeyChanged {
	fn display(&self, _data: &Data) -> String {
		format!("sudo key changed {:?}", self)
	}
}
impl DisplayEvent for runtime::balances::events::Transfer {
	fn display(&self, data: &Data) -> String {
		format!(
			"transfered {} ({} → {})",
			data.format_balance(self.amount),
			self.from,
			self.to
		)
	}
}
impl DisplayEvent for runtime::utility::events::BatchCompleted {
	fn display(&self, _data: &Data) -> String {
		format!("batch completed {:?}", self)
	}
}
impl DisplayEvent for runtime::sudo::events::Sudid {
	fn display(&self, _data: &Data) -> String {
		format!("SUDO call succeeded {:?}", self)
	}
}
impl DisplayEvent for runtime::technical_committee::events::Proposed {
	fn display(&self, _data: &Data) -> String {
		format!("proposed {:?}", self)
	}
}
