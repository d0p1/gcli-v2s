use crate::*;

/// define sudo subcommands
#[derive(Clone, Default, Debug, clap::Parser)]
pub enum Subcommand {
	/// Nothing
	#[default]
	#[clap(hide = true)]
	Nothing,
	/// set sudo keys
	SetKey { new_key: AccountId },
	/// force valid distance status
	SetDistanceOk { identity: IdtyId },
}

/// handle smith commands
pub async fn handle_command(data: Data, command: Subcommand) -> Result<(), GcliError> {
	let data = data.build_client().await?;
	match command {
		Subcommand::Nothing => todo!(),
		Subcommand::SetKey { new_key } => {
			set_key(&data, new_key).await?;
		}
		Subcommand::SetDistanceOk { identity } => {
			set_distance_ok(&data, identity).await?;
		}
	};

	Ok(())
}

/// set sudo key
pub async fn set_key(data: &Data, new_key: AccountId) -> Result<(), subxt::Error> {
	submit_call_and_look_event::<
		runtime::sudo::events::KeyChanged,
		Payload<runtime::sudo::calls::types::SetKey>,
	>(data, &runtime::tx().sudo().set_key(new_key.into()))
	.await
}

/// set distance ok
pub async fn set_distance_ok(data: &Data, identity: IdtyId) -> Result<(), subxt::Error> {
	let inner = runtime::distance::Call::force_valid_distance_status { identity };
	let inner = runtime::Call::Distance(inner);
	submit_call_and_look_event::<
		runtime::sudo::events::Sudid,
		Payload<runtime::sudo::calls::types::Sudo>,
	>(data, &runtime::tx().sudo().sudo(inner))
	.await
}
