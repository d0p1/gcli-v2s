use crate::*;

/// request distance evaluation
pub async fn request_distance_evaluation(data: &Data) -> Result<(), subxt::Error> {
	submit_call_and_look_event::<
		runtime::distance::events::EvaluationRequested,
		Payload<runtime::distance::calls::types::RequestDistanceEvaluation>,
	>(
		data,
		&runtime::tx().distance().request_distance_evaluation(),
	)
	.await
}

/// request distance evaluation for someone else (must be unvalidated)
pub async fn request_distance_evaluation_for(
	data: &Data,
	target: IdtyId,
) -> Result<(), subxt::Error> {
	submit_call_and_look_event::<
		runtime::distance::events::EvaluationRequested,
		Payload<runtime::distance::calls::types::RequestDistanceEvaluationFor>,
	>(
		data,
		&runtime::tx()
			.distance()
			.request_distance_evaluation_for(target),
	)
	.await
}
