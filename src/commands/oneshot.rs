use crate::*;

/// define oneshot account subcommands
#[derive(Clone, Default, Debug, clap::Parser)]
pub enum Subcommand {
	/// get balance of oneshot account
	#[default]
	Balance,
	/// create a oneshot account
	Create { balance: u64, dest: AccountId },
	/// consume a oneshot account
	Consume {
		dest: AccountId,
		#[clap(long = "oneshot")]
		dest_oneshot: bool,
	},
	/// consume a oneshot account whith remaining sent to an other account
	ConsumeWithRemaining {
		balance: u64,
		dest: AccountId,
		#[clap(long = "one")]
		dest_oneshot: bool,
		remaining_to: AccountId,
		#[clap(long = "rem-one")]
		remaining_to_oneshot: bool,
	},
}

/// handle oneshot commands
pub async fn handle_command(data: Data, command: Subcommand) -> Result<(), GcliError> {
	// build indexer because it is needed for all subcommands
	let mut data = data.build_client().await?;
	// match subcommand
	match command {
		Subcommand::Balance => oneshot_account_balance(&data).await?,
		Subcommand::Create { balance, dest } => {
			data = data.build_client().await?;
			create_oneshot_account(&data, balance, dest).await?;
		}
		Subcommand::Consume { dest, dest_oneshot } => {
			data = data.build_client().await?;
			consume_oneshot_account(&data, dest, dest_oneshot).await?;
		}
		Subcommand::ConsumeWithRemaining {
			balance,
			dest,
			dest_oneshot,
			remaining_to,
			remaining_to_oneshot,
		} => {
			data = data.build_client().await?;
			consume_oneshot_account_with_remaining(
				&data,
				balance,
				dest,
				dest_oneshot,
				remaining_to,
				remaining_to_oneshot,
			)
			.await?;
		}
	};

	Ok(())
}

/// get balance of oneshot account
pub async fn oneshot_account_balance(data: &Data) -> Result<(), anyhow::Error> {
	println!(
		"balance of oneshot account {} is: {}",
		data.address(),
		data.client()
			.storage()
			.at_latest()
			.await?
			.fetch(
				&runtime::storage()
					.oneshot_account()
					.oneshot_accounts(data.address()),
			)
			.await?
			.unwrap_or(0)
	);

	Ok(())
}

/// create oneshot account
pub async fn create_oneshot_account(
	data: &Data,
	balance: u64,
	dest: AccountId,
) -> Result<(), subxt::Error> {
	submit_call_and_look_event::<
		runtime::oneshot_account::events::OneshotAccountCreated,
		Payload<runtime::oneshot_account::calls::types::CreateOneshotAccount>,
	>(
		data,
		&runtime::tx()
			.oneshot_account()
			.create_oneshot_account(dest.into(), balance),
	)
	.await
}

/// consume oneshot account
pub async fn consume_oneshot_account(
	data: &Data,
	dest: AccountId,
	dest_oneshot: bool,
) -> Result<(), subxt::Error> {
	let client = data.client();
	let number = client
		.storage()
		.at_latest()
		.await?
		.fetch(&runtime::storage().system().number())
		.await?
		.unwrap();
	let payload = &runtime::tx().oneshot_account().consume_oneshot_account(
		number,
		if dest_oneshot {
			runtime::runtime_types::pallet_oneshot_account::types::Account::Oneshot(dest.into())
		} else {
			runtime::runtime_types::pallet_oneshot_account::types::Account::Normal(dest.into())
		},
	);
	submit_call_and_look_event::<
		runtime::oneshot_account::events::OneshotAccountConsumed,
		Payload<runtime::oneshot_account::calls::types::ConsumeOneshotAccount>,
	>(data, payload)
	.await
}

/// consume oneshot account with remaining
pub async fn consume_oneshot_account_with_remaining(
	data: &Data,
	balance: u64,
	dest: AccountId,
	dest_oneshot: bool,
	remaining_to: AccountId,
	remaining_to_oneshot: bool,
) -> Result<(), subxt::Error> {
	let client = data.client();

	let number = client
		.storage()
		.at_latest()
		.await?
		.fetch(&runtime::storage().system().number())
		.await?
		.unwrap();

	let payload = &runtime::tx()
		.oneshot_account()
		.consume_oneshot_account_with_remaining(
			number,
			if dest_oneshot {
				runtime::runtime_types::pallet_oneshot_account::types::Account::Oneshot(dest.into())
			} else {
				runtime::runtime_types::pallet_oneshot_account::types::Account::Normal(dest.into())
			},
			if remaining_to_oneshot {
				runtime::runtime_types::pallet_oneshot_account::types::Account::Oneshot(
					remaining_to.into(),
				)
			} else {
				runtime::runtime_types::pallet_oneshot_account::types::Account::Normal(
					remaining_to.into(),
				)
			},
			balance,
		);

	submit_call_and_look_event::<
		runtime::oneshot_account::events::OneshotAccountConsumed,
		Payload<runtime::oneshot_account::calls::types::ConsumeOneshotAccountWithRemaining>,
	>(data, payload)
	.await
}
