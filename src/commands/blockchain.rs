use crate::*;

/// define blockchain subcommands
#[derive(Clone, Default, Debug, clap::Parser)]
pub enum Subcommand {
	#[clap(hide = true)]
	Repart {
		// Number of transactions per block to target
		target: u32,
		#[clap(short = 'o', long = "old-repart")]
		// Old/actual repartition
		actual_repart: Option<u32>,
	},
	#[clap(hide = true)]
	SpamRoll { actual_repart: usize },
	/// Get information about runtime
	RuntimeInfo,
	/// Check current block
	#[default]
	CurrentBlock,
	/// Create one block manually (manual sealing)
	#[clap(hide = true)]
	CreateBlock,
}

/// handle blockchain commands
pub async fn handle_command(data: Data, command: Subcommand) -> Result<(), GcliError> {
	let mut data = data.build_client().await?;
	match command {
		Subcommand::Repart {
			target,
			actual_repart,
		} => commands::net_test::repart(&data, target, actual_repart).await?,
		Subcommand::SpamRoll { actual_repart } => {
			commands::net_test::spam_roll(&data, actual_repart).await?
		}
		Subcommand::RuntimeInfo => {
			data = data.fetch_system_properties().await?;
			commands::runtime::runtime_info(data).await;
		}
		Subcommand::CurrentBlock => {
			let finalized_number = fetch_finalized_number(&data).await?;
			let current_number = fetch_latest_number_and_hash(&data).await?.0;
			println!("on {}", data.cfg.duniter_endpoint);
			println!("finalized block\t{}", finalized_number);
			println!("current block\t{}", current_number);
		}
		Subcommand::CreateBlock => {
			todo!()
			// data.client()
			// 	.backend()
			// 	.call("engine_createBlock", subxt::backend::rpc::rpc_params![true, true]) // create empty block and finalize
			// 	.await?; // FIXME this gives a serialization error
		}
	}
	Ok(())
}

/// get genesis hash
pub async fn fetch_genesis_hash(data: &Data) -> Result<Hash, subxt::Error> {
	Ok(data
		.client()
		.storage()
		.at_latest()
		.await?
		.fetch(&runtime::storage().system().block_hash(0))
		.await?
		.unwrap())
}

/// get finalized number
pub async fn fetch_finalized_number(data: &Data) -> Result<BlockNumber, subxt::Error> {
	Ok(data
		.client()
		.storage()
		.at_latest()
		.await?
		.fetch(&runtime::storage().system().number())
		.await?
		.unwrap())
}

/// get finalized hash and number (require legacy)
pub async fn fetch_finalized_number_and_hash(
	data: &Data,
) -> Result<(BlockNumber, Hash), subxt::Error> {
	let hash = data
		.legacy_rpc_methods()
		.await
		.chain_get_finalized_head()
		.await?;
	let number = data
		.legacy_rpc_methods()
		.await
		.chain_get_block(Some(hash))
		.await?
		.unwrap()
		.block
		.header
		.number;

	Ok((number, hash))
}

/// get latest hash and number
pub async fn fetch_latest_number_and_hash(
	data: &Data,
) -> Result<(BlockNumber, Hash), subxt::Error> {
	let number = data
		.legacy_rpc_methods()
		.await
		.chain_get_header(None)
		.await?
		.unwrap()
		.number;
	let hash = data
		.legacy_rpc_methods()
		.await
		.chain_get_block_hash(Some(number.into()))
		.await?
		.unwrap();
	Ok((number, hash))
}
