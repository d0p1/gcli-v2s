use crate::*;

#[cfg(any(feature = "dev", feature = "gdev"))] // find how to get runtime calls
type Call = runtime::runtime_types::gdev_runtime::RuntimeCall;
type BalancesCall = runtime::runtime_types::pallet_balances::pallet::Call;

/// transfer balance to target
pub async fn transfer(
	data: &Data,
	balance: u64,
	dest: AccountId,
	keep_alive: bool,
	is_ud: bool,
) -> Result<(), subxt::Error> {
	match (keep_alive, is_ud) {
		(true, false) => {
			submit_call_and_look_event::<
				runtime::balances::events::Transfer,
				Payload<runtime::balances::calls::types::TransferKeepAlive>,
			>(
				data,
				&runtime::tx()
					.balances()
					.transfer_keep_alive(dest.into(), balance),
			)
			.await
		}
		(false, false) => {
			submit_call_and_look_event::<
				runtime::balances::events::Transfer,
				Payload<runtime::balances::calls::types::TransferAllowDeath>,
			>(
				data,
				&runtime::tx()
					.balances()
					.transfer_allow_death(dest.into(), balance),
			)
			.await
		}
		(true, true) => {
			submit_call_and_look_event::<
				runtime::balances::events::Transfer,
				Payload<runtime::universal_dividend::calls::types::TransferUdKeepAlive>,
			>(
				data,
				&runtime::tx()
					.universal_dividend()
					.transfer_ud_keep_alive(dest.into(), balance),
			)
			.await
		}
		(false, true) => {
			submit_call_and_look_event::<
				runtime::balances::events::Transfer,
				Payload<runtime::universal_dividend::calls::types::TransferUd>,
			>(
				data,
				&runtime::tx()
					.universal_dividend()
					.transfer_ud(dest.into(), balance),
			)
			.await
		}
	}
}

/// transfer balance to multiple target
pub async fn transfer_multiple(
	data: &Data,
	amount: u64,
	dests: Vec<AccountId>,
) -> Result<(), subxt::Error> {
	// build the list of transactions from the destination accounts
	let transactions: Vec<Call> = dests
		.into_iter()
		.map(|dest| {
			Call::Balances(BalancesCall::transfer_keep_alive {
				dest: dest.into(),
				value: amount,
			})
		})
		.collect();
	// wrap these calls in a batch call
	submit_call_and_look_event::<
		runtime::utility::events::BatchCompleted,
		Payload<runtime::utility::calls::types::Batch>,
	>(data, &runtime::tx().utility().batch(transactions))
	.await
}
