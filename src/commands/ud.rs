use crate::*;

/// define universal dividends subcommands
#[derive(Clone, Default, Debug, clap::Parser)]
pub enum Subcommand {
	#[default]
	/// Claim uds
	Claim,
}

/// handle ud commands
pub async fn handle_command(data: Data, command: Subcommand) -> Result<(), GcliError> {
	// build indexer because it is needed for all subcommands
	let data = data.build_client().await?.fetch_system_properties().await?;
	// match subcommand
	match command {
		Subcommand::Claim => {
			claim_ud(&data).await?;
		}
	};

	Ok(())
}

/// claim universal dividend
pub async fn claim_ud(data: &Data) -> Result<(), subxt::Error> {
	submit_call_and_look_event::<
		runtime::universal_dividend::events::UdsClaimed,
		Payload<runtime::universal_dividend::calls::types::ClaimUds>,
	>(data, &runtime::tx().universal_dividend().claim_uds())
	.await
}
