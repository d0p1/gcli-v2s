# Docker

Docker image for gcli is useful for people with docker-based infrastructure or old system with old glibc or libssl.

## Build

```sh
./docker/build.sh
```

## Use

To use gcli, run it with docker:

```sh
docker run --rm gcli
```

Ğcli uses folders to store config and vault. You can bind local folders to these locations:

```sh
# create folders
mkdir -p $HOME/.config/gcli/
mkdir -p $HOME/.local/share/gcli/
# run gcli in docker with there folders
docker run --rm \
--mount type=bind,src=$HOME/.config/gcli/,dst=/root/.config/gcli \
--mount type=bind,src=$HOME/.local/share/gcli/,dst=/root/.local/share/gcli \
gcli
```

An alias can be useful:

```sh
# define alias
alias docker_gcli="docker run --rm \
--mount type=bind,src=$HOME/.config/gcli/,dst=/root/.config/gcli \
--mount type=bind,src=$HOME/.local/share/gcli/,dst=/root/.local/share/gcli \
gcli"
# then simply call
docker_gcli --help
```
