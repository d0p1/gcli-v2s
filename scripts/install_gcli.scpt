-- Ask user if they want to install gcli
set userResponse to display dialog "Do you want to install gcli to /usr/local/bin?" buttons {"No", "Yes"} default button "Yes"

if button returned of userResponse is "Yes" then
    -- Run shell script to move gcli with administrator privileges
    try
        do shell script "mv gcli.app/Contents/MacOS/gcli /usr/local/bin" with administrator privileges
        display dialog "gcli has been installed to /usr/local/bin and is now available in your PATH."
    on error errMsg number errorNumber
        if errorNumber is -128 then
            display dialog "Installation cancelled."
        else
            display dialog "An error occurred: " & errMsg
        end if
    end try
else
    display dialog "Installation cancelled."
end if
